<?

namespace Nebula;

abstract class Nebula_Route_Caller_Options
{
	protected static $renderFile = 'index.twig';
	protected static $title = 'Nebula Page Title';

	public static function getOptionsArray()
	{
		return (new \ReflectionClass(__CLASS__))->getStaticProperties();
	}
}