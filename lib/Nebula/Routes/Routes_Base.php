<?php

namespace Nebula\Routes;

use Nebula\Nebula;
use Nebula\Nebula_Route_Caller;
use Nebula\Nebula_Router;

class Routes_Base
{
	protected $router;

	protected $nebula;

	public function __construct(Nebula_Router &$router, Nebula $nebula)
	{
		$this->router = $router;
		$this->nebula = $nebula;
	}

	protected function getRouter()
	{
		return $this->router;
	}

	protected function getNebula()
	{
		return $this->nebula;
	}

	protected function executeRoute(array $options, $method = null)
	{
		if (!empty($method)) {
			$options['method'] = $method;
		}

		if (empty($options['method'])) {
			throw new \Exception('No method provided for route');
		}

		$this->getRouter()->nrc = new Nebula_Route_Caller($this->getNebula(), [], $options);

		return $this->getRouter()->getNebulaRouteCaller()->getResponse();
	}
}