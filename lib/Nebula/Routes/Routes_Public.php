<?php

namespace Nebula\Routes;

use Aura\Router\Map;
use Zend\Diactoros\ServerRequest;
use Nebula\{Nebula_Config, Nebula_Router, Nebula_Route_Caller};

class Routes_Public extends Routes_Base implements Routes_Interface
{
	/**
	 * Route options
	 *
	 * @var array $this->_defaults
	 */
	private $_defaults;
	
	public function getRoutes()
	{
		$this->_defaults['title'] = 'Test Title';
		$this->_defaults['controller'] = \Nebula\Controller\TestController::class;
		$this->_defaults['baseTemplate'] = Nebula_Config::TWIG_TEMPLATE_BASE;

		/** @see TestController::testFunction() */
		$this->getRouter()->getRouteCollection()->get('public.read', '/{id}', function (ServerRequest $request)
		{
			$this->_defaults['method'] = 'testFunction';
			$this->_defaults['requiresAuthentication'] = true;
			$this->getRouter()->nrc = new Nebula_Route_Caller($this->getNebula(), $request, $this->_defaults);

			return $this->getRouter()->getNebulaRouteCaller()->getResponse();
		})->tokens(['id' => '\d+']);

		/** @see TestController::testFunction2() */
		$this->getRouter()->getRouteCollection()->get('read2', '/{id}/{id2}', function (ServerRequest $request)
		{
			$this->_defaults['method'] = 'testFunction2';
			$this->getRouter()->nrc = new Nebula_Route_Caller($this->getNebula(), $request, $this->_defaults);

			return $this->getRouter()->getNebulaRouteCaller()->getResponse();
		})->tokens(['id' => '\d+', 'id2' => '\d+']);

		/** @see TestController::ajaxTest() */
		$this->getRouter()->getRouteCollection()->post('ajaxTest', '/ajaxTest{format}', function (ServerRequest $request)
		{
			$this->_defaults['method'] = 'ajaxTest';
			$this->getRouter()->nrc = new Nebula_Route_Caller($this->getNebula(), $request, $this->_defaults);

			return $this->getRouter()->getNebulaRouteCaller()->getResponse();
		})->tokens(['format' => '.json']);

		/** @see TestController::ajaxTestHTML() */
		$this->getRouter()->getRouteCollection()->post('ajaxTestHTML', '/ajaxTestHTML{format}', function (ServerRequest $request)
		{
			$this->_defaults['method'] = 'ajaxTestHTML';
			$this->getRouter()->nrc = new Nebula_Route_Caller($this->getNebula(), $request, $this->_defaults);

			return $this->getRouter()->getNebulaRouteCaller()->getResponse();
		})->tokens(['format' => '.html']);
	}
}