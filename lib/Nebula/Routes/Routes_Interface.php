<?php

namespace Nebula\Routes;

use Aura\Router\Map;
use Nebula\Nebula_Router;

interface Routes_Interface
{
	public function getRoutes();
}