<?php

namespace Nebula;

class Nebula_Twig_Loader
{
	public $twig;

	public function __construct()
	{
		$loader = new \Twig_Loader_Filesystem(Nebula_Config::TWIG_FILESYSTEM_BASE);
		$this->twig = new \Twig_Environment($loader, ['debug' => true]);
		$this->twig->addExtension(new \Twig_Extension_Debug());

		// Load twig functions
		$this->twigFunctions();

		// Load twig filters
		$this->twigFilters();
	}

	public function getTwig()
	{
		return $this->twig;
	}

	private function twigFunctions()
	{
		$this->twig->addFunction(new \Twig_Function(
			'baseURL',
			function () {
				$protocol = 'http://';
				if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') {
					$protocol = 'https://';
				}
				return $protocol . $_SERVER['SERVER_NAME'];
			}
		));

		$this->twig->addFunction(new \Twig_Function(
			'sum',
			function ($value1 = 0, $value2 = 0) {
				return $value1 + $value2;
			}
		));

		$this->twig->addFunction(new \Twig_Function(
			'multiply',
			function ($value1 = 0, $value2 = 0) {
				return $value1 * $value2;
			}
		));

		$this->twig->addFunction(new \Twig_Function(
			'createToken',
			function (int $length, string $charset = Nebula_Config::TWIG_RANDOM_STRING_CHARSET) {
				$output = '';

				for ($i = 0; $i < $length; $i++) {
					mt_srand(random_int(PHP_INT_MIN, PHP_INT_MAX));
					$charset = str_shuffle($charset);
					$output .= $charset{random_int(0, strlen($charset) - 1)};
				}
				return $output;
			}
		));

		$this->twig->addFunction(new \Twig_Function(
			'createTokenFormatted',
			function (int $length, int $segments = 4, string $charset = Nebula_Config::TWIG_RANDOM_STRING_CHARSET) {
				$output = call_user_func($this->twig->getFunction('createToken')->getCallable(), $length, $charset);
				$output = str_split($output, ceil($length / $segments));
				$output = implode('-', $output);

				return $output;
			}
		));

		return $this;
	}

	private function twigFilters()
	{
		$this->twig->addFilter(new \Twig_Filter(
			'hash',
			function ($value) {
				return hash('sha256', $value);
			}
		));

		$this->twig->addFilter(new \Twig_Filter(
			'dump',
			function ($value) {
				return var_dump($value);
			}
		));

		return $this;
	}
}