<?php

namespace Nebula\Controller;

use Nebula\Model\TestModel;

class TestController extends BaseController
{
	public function testFunction()
	{
		$id = $this->getToken('id');
		$get = $this->getGet('test');

		$string = "You wanted ID number " . $id;

		$m = new TestModel($this->getState());
		$result = $m->testModelFunction();

		$this->getState()->setRenderData('adminUser', $result);

		$this->getState()->setRenderData('result', $string);
		$this->getState()->setRenderData('test', $get);

		// If renderfile is specified in controller method, it will over-ride
		// renderfile specified in the route options
		$this->getState()->setRenderFile('public/route_test.twig');
	}

	public function testFunction2()
	{
		$this->getState()->setRenderData('id', $this->getToken('id'));
		$this->getState()->setRenderData('id2', $this->getToken('id2'));
		$this->getState()->setRenderFile('public/route_test2.twig');
	}

	public function ajaxTest()
	{
		$this->renderJSON(['name' => 'test']);
	}

	public function ajaxTestHTML()
	{
		$data[] = [
			'name' => $this->getPost('name'),
			'email' => $this->getPost('email')
		];
		$this->getState()->setRenderData('data', $data);
		$this->getState()->setRenderFile('public/ajaxTestHTML.twig');
	}
}