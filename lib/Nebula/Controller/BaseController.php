<?php

namespace Nebula\Controller;

use Nebula\Nebula;
use Nebula\Nebula_Config;

class BaseController
{
	protected $_state;

	protected $_options;

	public function __construct(Nebula $obj, array $options = [])
	{
		$this->_state = $obj;
		$this->_options = $options;
		$this->getState()->setRenderData('__options__', $this->getOptions());
	}

	protected function getState()
	{
		return $this->_state;
	}

	protected function getOptions()
	{
		return $this->_options;
	}

	/**
	 * @param $key
	 * @return mixed
	 * Return value from $_POST
	 */
	public function getPost($key)
	{
		return $this->getState()->getPost()[$key];
	}

	/**
	 * @param $key
	 * @return mixed
	 * Return value from $_GET
	 */
	public function getGet($key)
	{
		return $this->getState()->getGet()[$key];
	}

	/**
	 * @param $key
	 * @return mixed
	 * Return value from URL token array
	 */
	public function getToken($key)
	{
		return $this->getState()->getToken()[$key];
	}

	/**
	 * @param $data
	 * @return $this
	 * Will output JSON to default blank json template
	 */
	public function renderJSON($data)
	{
		$this->getState()->setRenderData('__json__', json_encode($data));
		$this->getState()->setRenderFile(Nebula_Config::TWIG_TEMPLATE_JSON);
		return $this;
	}
}