<?php
namespace Nebula;

abstract class Nebula_Config
{
	/**
	 * TODO - Don't set const values here. Just initialise the const and populate values from database
	 */

	/**
	 * Twig Defaults
	 */
	public const TWIG_FILESYSTEM_BASE = 'View';
	public const TWIG_TEMPLATE_BASE = 'public/index.twig';
	public const TWIG_TEMPLATE_SPECIAL = 'tools/base.twig';
	public const TWIG_TEMPLATE_JSON = 'tools/json.twig';
	public const TWIG_RANDOM_STRING_CHARSET = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
}