<?php

namespace Nebula;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\DriverManager;

class Nebula_Database_Loader
{
	public $db;

	public function __construct(string $dbHost, string $dbName, string $dbUser, string $dbPass)
	{
		try {
			$this->db = DriverManager::getConnection([
				'dbname' => $dbName,
				'user' => $dbUser,
				'password' => $dbPass,
				'host' => $dbHost,
				'driver' => 'pdo_mysql',
				'charset' => 'utf8mb4'
			], new Configuration());
		} catch (\Exception $e) {
			echo $e->getMessage(); die;
		}
	}

	public function getConnection()
	{
		return $this->db;
	}
}