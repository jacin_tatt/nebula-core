<?php

namespace Nebula;

use Zend\Diactoros\ServerRequest;

class Nebula_Route_Caller
{
	public $response;

	public $options;

	protected $formatsWithSpecialTemplate =
	[
		'.json',
		'.nebula',
		'.html'
	];

	public function __construct(Nebula $nebula, ServerRequest $request = NULL, array $options = [])
	{
		if (isset($request)) {
			$this->setRequestValues($nebula, $request);
			$this->setOptions($options);

			if (!empty($this->options['controller']) && !empty($this->options['method'])) {
				try {
					$class = new $this->options['controller']($nebula, $options);
					$class->{$this->options['method']}();
				} catch (\Exception $e) {
					echo $e;
				}
			}

			if (!empty($this->options['renderFile'])) {
				if (empty($nebula->getRenderFile())) {
					$nebula->setRenderFile($this->options['renderFile']);
				}
			}

			/** If the URL extension (ie .json) matches the $formatsWithSpecialTemplate array, then set special baseTemplate */
			if ($this->formatRequiresSpecialTemplate($request)) {
				$this->setSpecialTemplate($request);
			}
			$this->setResponse($nebula);
		}
	}

	protected function setRequestValues(Nebula $nebula, ServerRequest $request)
	{
		$nebula->setPost($request->getParsedBody());
		$nebula->setGet($request->getQueryParams());
		$nebula->setToken($request->getAttributes());
	}

	protected function setResponse(Nebula $instance)
	{
		$this->response = new \Zend\Diactoros\Response();
		$this->response->getBody()->write($instance->getRender());
	}

	public function getResponse()
	{
		return $this->response;
	}

	private function setOptions(array $options = [])
	{
		$this->options = $this->getDefaultOptions($options);

		return $this;
	}

	private function getDefaultOptions(array $options = [])
	{
		$defaults = Nebula_Route_Caller_Options::getOptionsArray();

		foreach ($defaults as $key => $val) {
			if (!array_key_exists($key, $options)) {
				$options[$key] = $val;
			}
		}
		return $options;
	}

	public function getOptions()
	{
		return $this->options;
	}

	public function getFormatsWithSpecialTemplate()
	{
		return $this->formatsWithSpecialTemplate;
	}

	private function formatRequiresSpecialTemplate(ServerRequest $request)
	{
		return in_array($request->getAttribute('format'), $this->getFormatsWithSpecialTemplate());
	}

	/**
	 * @param ServerRequest $request
	 * Specify URL extensions that should use a special template
	 */
	private function setSpecialTemplate(ServerRequest $request)
	{
		$specialTemplate = '';

		switch ($request->getAttribute('format'))
		{
			case '.json':
			case '.nebula':
			case '.html':
			case '.htm':
			case '.phtml':
			case '.asp':
				$specialTemplate = Nebula_Config::TWIG_TEMPLATE_SPECIAL;
				break;
		}

		$this->setOptions(['baseTemplate' => $specialTemplate]);
	}
}