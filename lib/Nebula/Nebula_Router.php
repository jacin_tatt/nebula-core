<?php

namespace Nebula;

use Nebula\Routes\Routes_Admin;
use Nebula\Routes\Routes_Interface;
use Nebula\Routes\Routes_Public;
use \Zend\Diactoros\ServerRequestFactory as ServerRequestFactory;
use \Zend\Diactoros\ServerRequest as ServerRequest;

class Nebula_Router
{
	protected $request;

	protected $routerContainer;

	protected $routeCollection;

	protected $routeMatcher;

	protected $route;

	protected $callable;

	protected $response;

	public $output;
	public $routeProperties;

	/** @var Nebula_Route_Caller */
	public $nrc;

	private $nebula;

	public function __construct(Nebula $nebula)
	{
		$this->nebula = $nebula;

		$this->request = ServerRequestFactory::fromGlobals(
			$_SERVER,
			$_GET,
			$_POST,
			$_COOKIE,
			$_FILES
		);

		$this->routerContainer = new \Aura\Router\RouterContainer();
		$this->routeCollection = $this->routerContainer->getMap();

		$this->routeCollection();
	}

	public function matchRoute()
	{
		$this->routeMatcher = $this->routerContainer->getMatcher();
		$this->route = $this->routeMatcher->match($this->request);

		if (! $this->route) {
			echo "No route found for the request: ";
			echo $_SERVER['REQUEST_URI'];
			exit;
		}

		foreach ($this->route->attributes as $key => $val) {
			$this->request = $this->request->withAttribute($key, $val);
		}

		$callable = $this->route->handler;
		$this->response = $callable($this->request, $this->response);

		foreach ($this->response->getHeaders() as $name => $values) {
			foreach ($values as $value) {
				header(sprintf('%s: %s', $name, $value), false);
			}
		}
		$this->output = $this->response->getBody();

		$this->setMatchedRouteProperties();
	}

	protected function setRequestValues(ServerRequest $request)
	{
		$this->_nebulaPost = $request->getParsedBody();
		$this->_nebulaGet = $request->getQueryParams();
		$this->_nebulaToken = $request->getAttributes();
	}

	public function loadRouteCollection(Routes_Interface $class)
	{
		$class->getRoutes();
	}

	/**
	 * Populate route collection
	 */
	protected function routeCollection()
	{
		$this->getRouteCollection()->attach('public.', '/new', function () {
			(new Routes_Public($this, $this->nebula))->getRoutes();
		});
	}

	protected function setMatchedRouteProperties()
	{
		$matchedRoute = $this->routeMatcher->getMatchedRoute();
		$this->routeProperties = [];
		$this->routeProperties['name'] = $matchedRoute->name;
		$this->routeProperties['namePrefix'] = $matchedRoute->namePrefix;
		$this->routeProperties['path'] = $matchedRoute->path;
		//echo "<pre>"; print_r($matchedRoute->path); die;
	}

	/**
	 * @return Nebula_Route_Caller
	 * If $this->nrc is not valid, return empty route caller to prevent error
	 */
	public function getNebulaRouteCaller()
	{
		return $this->nrc ?? new Nebula_Route_Caller($this->nebula);
	}

	public function getOutput()
	{
		return $this->output;
	}

	public function getRouteProperties()
	{
		return $this->routeProperties;
	}

	public function getRouteCollection()
	{
		return $this->routeCollection;
	}
}