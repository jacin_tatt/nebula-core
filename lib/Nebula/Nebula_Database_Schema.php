<?php

namespace Nebula;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Comparator;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\SchemaConfig;

class Nebula_Database_Schema extends Nebula_Database_Loader
{
	/**
	 * @var \Doctrine\DBAL\Schema\AbstractSchemaManager
	 */
	private $schemaManager;

	/**
	 * @var \Doctrine\DBAL\Platforms\AbstractPlatform
	 */
	private $platform;

	/**
	 * @var Schema
	 */
	private $fromSchema;

	/**
	 * @var Schema
	 */
	private $toSchema;

	/**
	 * @var Comparator
	 */
	private $comparator;

	public function __construct()
	{
		parent::__construct();

		$this->schemaManager = $this->getConnection()->getSchemaManager();
		$this->platform = $this->getConnection()->getDatabasePlatform();
		$this->fromSchema = $this->getFromSchema();
		$this->toSchema = $this->getToSchema();
		$this->comparator = new Comparator();

		$this->defineSchema();
	}

	private function getSchemaManager()
	{
		return $this->schemaManager;
	}

	private function getComparator()
	{
		return $this->comparator;
	}

	private function getPlatform()
	{
		return $this->platform;
	}

	private function getFromSchema()
	{
		return empty($this->fromSchema) ? $this->getSchemaManager()->createSchema() : $this->fromSchema;
	}

	private function getToSchema()
	{
		return empty($this->toSchema) ? new Schema() : $this->toSchema;
	}

	public function compareSchemas($safeSql = false)
	{
		$diff = $this->getComparator()->compare($this->getFromSchema(), $this->getToSchema());

		if ($safeSql) {
			$sql = $diff->toSaveSql($this->getPlatform());
		} else {
			$sql = $diff->toSql($this->getPlatform());
		}

		print_r(empty($sql) ? 'No SQL changes detected' . PHP_EOL : $sql);
	}

	public function executeSchema($safeSql = false)
	{
		$diff = $this->getComparator()->compare($this->getFromSchema(), $this->getToSchema());
		$totalExecutions = 0;

		if ($safeSql === true) {
			$sql = $diff->toSaveSql($this->getPlatform());
		} else {
			$sql = $diff->toSql($this->getPlatform());
		}

		foreach ($sql as $query) {
			$this->executeQuery($query);
			$totalExecutions++;
		}
		print_r('Completed ' . $totalExecutions . ' database queries' . PHP_EOL);
	}

	private function executeQuery($query)
	{
		if (!empty($query)) {
			try {
				$this->getConnection()->query($query);
			} catch (DBALException $e) {
				print_r($e->getMessage()); exit;
			}
		}
	}

	/**
	 * Defines database schema
	 */
	private function defineSchema()
	{
		$tbl_testTable = $this->getToSchema()->createTable('testTable');
		$tbl_testTable->addColumn('id', 'bigint', ['length' => 20, 'notnull' => true, 'autoincrement' => true]);
		$tbl_testTable->addColumn('field', 'string', ['length' => 100, 'notnull' => false]);
		$tbl_testTable->setPrimaryKey(['id']);
	}
}