<?php

namespace Nebula\Model;

use Nebula\Nebula;

class BaseModel extends Nebula
{
	protected $_state;

	public function __construct(Nebula $obj)
	{
		$this->_state = $obj;
	}

	protected function getState()
	{
		return $this->_state;
	}
}