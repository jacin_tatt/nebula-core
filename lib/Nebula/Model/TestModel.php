<?php

namespace Nebula\Model;

class TestModel extends BaseModel
{
	protected $tableName = 'adminUser';

	public function testModelFunction()
	{
		$q = $this->getState()->getDb()->createQueryBuilder()
			->select('*')
			->from($this->tableName);

		return $q->execute()->fetchAll(\PDO::FETCH_ASSOC);
	}
}