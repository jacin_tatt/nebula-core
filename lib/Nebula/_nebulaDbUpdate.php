<?php

chdir(__DIR__);
require_once('../vendor/autoload.php');
//require_once('_nebulaAutoload.php');
require_once('Nebula_Database_Loader.php');
require_once('Nebula_Database_Schema.php');

/**
 * Check for schema updates from the command line by calling:
 * php schema.php --mode=check --safe=true/false
 *
 * Perform schema updates from the command line by calling:
 * php schema.php --mode=update --safe=true/false
 *
 * Always run a check beforehand to ensure nothing bad will execute!
 *
 * If --safe = true, no SQL for dropping tables will be generated
 */

try {
	$schema = new \Nebula\Nebula_Database_Schema();

	$arguments = [];
	$safeSQL = true;

	if ($argc > 3) {
		throw new Exception('Too many arguments supplied');
	}

	$i = 0;

	foreach ($argv as $key => $val) {
		if ($i > 0) {
			$explode = explode('=', $val);

			if (!empty($arguments[$explode[0]])) {
				throw new Exception('Duplicate parameter ' . $explode[0]);
			} else {
				$arguments[$explode[0]] = $explode[1];
			}
		}
		$i++;
	}

	if (!isset($arguments['--mode']) || empty($arguments['--mode'])) {
		throw new Exception('The --mode parameter is missing');
	}

	if ($arguments['--mode'] !== 'check' && $arguments['--mode'] !== 'update') {
		throw new Exception('Invalid value supplied for --mode parameter: "check" or "update" required');
	}

	if (isset($arguments['--safe'])) {
		if ($arguments['--safe'] !== 'true' && $arguments['--safe'] !== 'false') {
			throw new Exception('Invalid value supplied for --safe parameter: "true" or "false" required');
		}

		if ($arguments['--safe'] == 'false') {
			$safeSQL = false;
		}
	}

	if ($arguments['--mode'] === 'check') {
		$schema->compareSchemas($safeSQL);
	} else if ($arguments['--mode'] === 'update') {
		$schema->executeSchema($safeSQL);
	}

} catch (Exception $e) {
	print_r($e->getMessage() . PHP_EOL); exit;
}
