<?php

namespace Nebula;

use Nebula\Nebula_Twig_Loader;
use Nebula\Nebula_Database_Loader;

class Nebula {

	/**
	 * @var \Doctrine\DBAL\Connection
	 */
	protected $db;

	/**
	 * @var \Twig_Environment
	 */
	public $twig;

	public $render;

	/**
	 * @var string
	 * Path to twig file relative to Nebula_Config::TWIG_FILESYSTEM_BASE
	 */
	public $renderFile;

	/**
	 * @var array
	 * Associative array of data to be passed to twig render file
	 */
	public $renderData;

	/**
	 * @var Nebula_Router
	 */
	public $_nebula_route;

	/**
	 * @var array $_nebulaPost
	 * Array of $_POST values
	 */
	protected $_nebulaPost;

	/**
	 * @var array $_nebulaGet
	 * Array of $_GET values
	 */
	protected $_nebulaGet;

	/**
	 * @var array $_nebulaToken
	 * Array of URL token values as defined in route
	 */
	protected $_nebulaToken;

	public function __construct()
	{
		$this->renderData = [];
		$this->twig = (new Nebula_Twig_Loader())->getTwig();
	}

	public function getTwig()
	{
		return $this->twig;
	}

	public function getRender()
	{
		return $this->twig->render($this->getRenderFile(), $this->getRenderData());
	}

	public function setRenderFile(string $path)
	{
		$this->renderFile = $path;
	}

	public function getRenderFile()
	{
		return $this->renderFile;
	}

	public function initRoute()
	{
		$this->_nebula_route = new Nebula_Router($this);
	}


	public function initDatabase(string $dbHost, string $dbName, string $dbUser, string $dbPass)
	{
		$this->db = (new Nebula_Database_Loader(
			$dbHost,
			$dbName,
			$dbUser,
			$dbPass
		))->getConnection();
	}

	public function getRoute()
	{
		return $this->_nebula_route;
	}

	public function getRouteOptions()
	{
		return $this->getRoute()->getNebulaRouteCaller()->getOptions();
	}

	public function setRenderData($key, $value)
	{
		$this->renderData[$key] = $value;
		return $this;
	}

	public function getRenderData()
	{
		return $this->renderData;
	}

	public function getDb()
	{
		return $this->db;
	}

	public function getPost()
	{
		return $this->_nebulaPost;
	}

	public function getGet()
	{
		return $this->_nebulaGet;
	}

	public function getToken()
	{
		return $this->_nebulaToken;
	}
	public function setPost(array $value)
	{
		$this->_nebulaPost = $value;
		return $this;
	}

	public function setGet(array $value)
	{
		$this->_nebulaGet = $value;
		return $this;
	}

	public function setToken(array $value)
	{
		$this->_nebulaToken = $value;
		return $this;
	}
}